package cz.janousek.work.ares.ui.companydetail;

import android.app.Activity;

import cz.janousek.work.ares.AresApplication;
import cz.janousek.work.ares.service.CompanyCallback;
import cz.janousek.work.ares.service.CompanyService;
import cz.janousek.work.ares.ui.entryform.listener.SearchButtonListener;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public class Factories {

    private final Activity activity;
    private final AresApplication ares;

    private Renderer renderer;

    public Factories(Activity activity, AresApplication ares) {
        this.activity = activity;
        this.ares = ares;
    }

    public Renderer getRenderer() {
        if (renderer == null) {
            renderer = new Renderer(activity);
        }
        return renderer;
    }
}
