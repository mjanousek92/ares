package cz.janousek.work.ares.ui.entryform.listener;

import android.view.View;

import cz.janousek.work.ares.service.CompanyService;
import cz.janousek.work.ares.ui.entryform.Renderer;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public class SearchButtonListener implements View.OnClickListener {

    private CompanyService companyService;
    private Renderer renderer;

    public SearchButtonListener(CompanyService companyService, Renderer renderer) {
        this.companyService = companyService;
        this.renderer = renderer;
    }

    @Override
    public void onClick(View v) {
        companyService.load(renderer.getIco());
    }

}
