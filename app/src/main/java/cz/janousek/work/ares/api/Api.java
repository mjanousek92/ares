package cz.janousek.work.ares.api;

import cz.janousek.work.ares.model.api.AresResponses;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Martin Janousek on 9.2.18.
 * - email: mjanousek92@gmail.com
 */

public interface Api {

    @GET("/cgi-bin/ares/darv_or.cgi")
    Call<AresResponses> get(@Query("ico") String ico);

}
