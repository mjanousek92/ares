package cz.janousek.work.ares.model.api;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import cz.janousek.work.ares.model.database.Company;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

@Root(strict = false, name = "Ares_odpovedi")
public class AresResponses {

    @Element(name = "EK", required = false)
    @Path("Odpoved/E")
    public String errorCode;

    @Element(name = "ET", required = false)
    @Path("Odpoved/E")
    public String errorMessage;

    @Element(name = "Vypis_OR", required = false)
    @Path("Odpoved")
    public CompanyApiModel registerRecord;

    public boolean containsError() {
        return errorCode != null || errorMessage != null;
    }

    public Company makeCompany() {
        return registerRecord.makeCompany();
    }
}
