package cz.janousek.work.ares.model.database;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import static cz.janousek.work.ares.model.database.Company.ColumnName.*;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

@DatabaseTable(tableName = "company")
public class Company {

    @DatabaseField(columnName = COLUMN_ID, id = true)
    private String ico;

    @DatabaseField(columnName = COLUMN_NAME)
    private String name;

    @DatabaseField(columnName = COLUMN_ADDRESS_STATE)
    private String addressState;

    @DatabaseField(columnName = COLUMN_ADDRESS_TOWN)
    private String addressTown;

    @DatabaseField(columnName = COLUMN_ADDRESS_STREET)
    private String addressStreet;

    @DatabaseField(columnName = COLUMN_ADDRESS_HOUSE_NUMBER)
    private String addressHouseNumber;

    @DatabaseField(columnName = COLUMN_ACTIVITIES, dataType = DataType.SERIALIZABLE)
    private String[] activities;

    public Company() {}

    public Company(String ico, String name, String addressState, String addressTown, String addressStreet, String addressHouseNumber, String[] activities) {
        this.ico = ico;
        this.name = name;
        this.addressState = addressState;
        this.addressTown = addressTown;
        this.addressStreet = addressStreet;
        this.addressHouseNumber = addressHouseNumber;
        this.activities = activities;
    }

    public String getIco() {
        return ico;
    }

    public String getName() {
        return name;
    }

    public String getAddressState() {
        return addressState;
    }

    public String getAddressTown() {
        return addressTown;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public String getAddressHouseNumber() {
        return addressHouseNumber;
    }

    public String[] getActivities() {
        return activities;
    }

    class ColumnName {
        static final String COLUMN_ID = "id";
        static final String COLUMN_NAME = "name";
        static final String COLUMN_ADDRESS_STATE = "address_state";
        static final String COLUMN_ADDRESS_TOWN = "address_town";
        static final String COLUMN_ADDRESS_STREET = "address_street";
        static final String COLUMN_ADDRESS_HOUSE_NUMBER = "address_house_number";
        static final String COLUMN_FOUNDER_NAME = "founder_name";
        static final String COLUMN_FOUNDER_SURNAME = "founder_surname";
        static final String COLUMN_ACTIVITIES = "activities";
    }

}
