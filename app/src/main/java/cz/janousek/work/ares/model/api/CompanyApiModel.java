package cz.janousek.work.ares.model.api;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

import cz.janousek.work.ares.model.database.Company;
import cz.janousek.work.ares.model.database.CompanyBuilder;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

@Root(strict = false, name = "Vypis_OR")
public class CompanyApiModel {

    @Element(name = "ICO", required = false)
    @Path("ZAU")
    private String ico;

    @Element(name = "OF", required = false)
    @Path("ZAU")
    private String name;

    @Element(name = "NS", required = false)
    @Path("ZAU/SI")
    private String addressState;

    @Element(name = "N", required = false)
    @Path("ZAU/SI")
    private String addressTown;

    @Element(name = "NU", required = false)
    @Path("ZAU/SI")
    private String addressStreet;

    @Element(name = "CD", required = false)
    @Path("ZAU/SI")
    private String addressHouseNumber;

    @ElementList(name = "PP", required = false, data = true)
    @Path("CIN")
    private List<String> activities;


    public Company makeCompany() {
        CompanyBuilder builder = new CompanyBuilder();
        builder.setIco(ico);
        builder.setName(name);
        builder.setAddressState(addressState);
        builder.setAddressTown(addressTown);
        builder.setAddressStreet(addressStreet);
        builder.setAddressHouseNumber(addressHouseNumber);
        builder.setActivities(activities.toArray(new String[activities.size()]));
        return builder.create();
    }
}
