package cz.janousek.work.ares.ui.companydetail;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import cz.janousek.work.ares.R;
import cz.janousek.work.ares.model.database.Company;
import cz.janousek.work.ares.ui.entryform.listener.SearchButtonListener;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public class Renderer {
    private ViewHolder views;
    private Activity activity;

    public Renderer(Activity activity) {
        this.activity = activity;
        init();
    }

    public void render(Company company) {
        showCompanyContent();
        renderCompanyName(company.getName());
        renderActitivities(company.getActivities());
        renderAddress(company);
    }

    private void renderAddress(Company company) {
        renderStreetAndHouseNumber(company.getAddressStreet(), company.getAddressHouseNumber());
        renderTown(company.getAddressTown());
        renderState(company.getAddressState());
    }

    private void renderState(String addressState) {
        views.tvAddressState.setText(addressState);
    }

    private void renderTown(String addressTown) {
        views.tvAddressTown.setText(addressTown);
    }

    private void renderStreetAndHouseNumber(String addressStreet, String addressHouseNumber) {
        views.tvAddressStreetAndHouseNumber.setText(addressStreet + ", " + addressHouseNumber);
    }

    private void renderActitivities(String[] activities) {
        List<String> preparedActivities = prepareActivities(activities);
        views.tvActivitiesContent.setText(TextUtils.join("\n", preparedActivities));
    }

    @NonNull
    private List<String> prepareActivities(String[] activities) {
        List<String> preparedActivities = new LinkedList();
        for (String activity : activities) {
            preparedActivities.add(activity.replace("\n", ""));
        }
        return preparedActivities;
    }

    private void renderCompanyName(String name) {
        views.ctlToobar.setTitle(name);
    }

    private void showCompanyContent() {
        views.clCompanyContent.setVisibility(View.VISIBLE);
        views.rlEmptyContent.setVisibility(View.GONE);
    }

    private void showEmptyContent() {
        views.clCompanyContent.setVisibility(View.GONE);
        views.rlEmptyContent.setVisibility(View.VISIBLE);
    }

    public void renderEmpty() {
        showEmptyContent();
    }

    private void init() {
        views = new ViewHolder(activity);
    }

    private class ViewHolder {

        private Toolbar toolbar;
        private CollapsingToolbarLayout ctlToobar;
        private RelativeLayout rlEmptyContent;
        private ConstraintLayout clCompanyContent;
        private TextView tvActivitiesContent,
                tvAddressStreetAndHouseNumber, tvAddressTown, tvAddressState;


        public ViewHolder(Activity activity) {
            init(activity);
        }

        private void init(Activity activity) {
            toolbar = activity.findViewById(R.id.toolbar);
            ctlToobar = activity.findViewById(R.id.toolbar_layout);
            rlEmptyContent = activity.findViewById(R.id.rl_empty_content);
            clCompanyContent = activity.findViewById(R.id.cl_company_content);
            tvActivitiesContent = activity.findViewById(R.id.tv_activities_content); 
            tvAddressStreetAndHouseNumber = activity.findViewById(R.id.tv_address_street_and_house_number); 
            tvAddressTown = activity.findViewById(R.id.tv_address_town); 
            tvAddressState = activity.findViewById(R.id.tv_address_state);
        }
    }
}
