package cz.janousek.work.ares.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import cz.janousek.work.ares.dao.CompanyDao;
import cz.janousek.work.ares.dao.CompanySqlDao;
import cz.janousek.work.ares.model.database.Company;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "ares.db";
    private static final int DATABASE_VERSION = 1;
    private CompanySqlDao companyDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Company.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        // Not necessary for demo app
    }

    @Override
    public void close() {
        companyDao = null;
        super.close();
    }

    public CompanyDao getCompanyDao() {
        if (companyDao == null) {
            try {
                companyDao = new CompanySqlDao(getConnectionSource());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return companyDao;
    }
}
