package cz.janousek.work.ares.service;

import java.sql.SQLException;

import cz.janousek.work.ares.AresApplication;
import cz.janousek.work.ares.api.Api;
import cz.janousek.work.ares.api.AresCallback;
import cz.janousek.work.ares.dao.CompanyDao;
import cz.janousek.work.ares.dao.CompanySqlDao;
import cz.janousek.work.ares.model.api.AresResponses;
import cz.janousek.work.ares.model.database.Company;
import retrofit2.Call;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public class CompanyService {
    Api api;
    CompanyDao companyDao;
    CompanyCallback companyCallback;

    public CompanyService(AresApplication ares, CompanyCallback companyCallback) {
        this.companyDao = ares.getDatabaseHelper().getCompanyDao();
        this.companyCallback = companyCallback;
        this.api = ares.getApi();
    }

    public void load(String ico) {
        try {
            loadFromDb(ico);
        } catch (CompanySqlDao.NotFoundException | SQLException e) {
            loadFromApi(ico);
        }
    }

    private void loadFromDb(String ico) throws SQLException {
        Company company = companyDao.get(ico);
        companyCallback.onSuccess(company);
    }

    private void loadFromApi(String ico) {
        AresCallback aresCallback = new AresCallback(this);
        Call<AresResponses> call = api.get(ico);
        call.enqueue(aresCallback);
    }

    public void proceedError(String errorMessage) {
        companyCallback.onFailure(errorMessage);
    }

    public void proceedCompany(Company company) {
        companyDao.save(company);
        companyCallback.onSuccess(company);
    }
}
