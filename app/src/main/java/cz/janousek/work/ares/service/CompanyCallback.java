package cz.janousek.work.ares.service;

import android.app.Activity;
import android.content.Intent;

import cz.janousek.work.ares.model.database.Company;
import cz.janousek.work.ares.ui.companydetail.CompanyDetailActivity;
import cz.janousek.work.ares.ui.entryform.Renderer;

import static cz.janousek.work.ares.ui.companydetail.CompanyDetailActivity.INTENT_ICO;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public class CompanyCallback {


    private Activity activity;
    private Renderer renderer;

    public CompanyCallback(Activity activity, Renderer renderer) {
        this.activity = activity;
        this.renderer = renderer;
    }

    public void onSuccess(Company company) {
        renderer.renderError(null);
        Intent intent = new Intent(activity, CompanyDetailActivity.class);
        intent.putExtra(INTENT_ICO, company.getIco());
        activity.startActivity(intent);
    }

    public void onFailure(String error) {
        renderer.renderError(error);
    }
}
