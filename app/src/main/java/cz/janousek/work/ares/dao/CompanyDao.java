package cz.janousek.work.ares.dao;

import cz.janousek.work.ares.model.database.Company;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public interface CompanyDao extends Dao<Company, String> {
}
