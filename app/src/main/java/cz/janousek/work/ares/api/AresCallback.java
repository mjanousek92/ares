package cz.janousek.work.ares.api;

import cz.janousek.work.ares.model.api.AresResponses;
import cz.janousek.work.ares.service.CompanyService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public class AresCallback implements Callback<AresResponses> {

    private CompanyService companyService;

    public AresCallback(CompanyService companyService) {
        this.companyService = companyService;
    }

    @Override
    public void onResponse(Call<AresResponses> call, Response<AresResponses> response) {
        AresResponses aresResponses = response.body();
        if (aresResponses.containsError()) {
            companyService.proceedError(aresResponses.errorMessage);
        } else {
            companyService.proceedCompany(aresResponses.makeCompany());
        }
    }

    @Override
    public void onFailure(Call<AresResponses> call, Throwable t) {
        throw new RuntimeException(t);
    }
}
