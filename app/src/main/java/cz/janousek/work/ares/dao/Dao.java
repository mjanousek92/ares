package cz.janousek.work.ares.dao;

import java.sql.SQLException;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public interface Dao<T, ID> {
    T get(ID id) throws SQLException;
    void save(T data);
}
