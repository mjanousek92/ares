package cz.janousek.work.ares.model.database;

import java.util.List;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public class CompanyBuilder{

    private String ico;
    private String addressState;
    private String addressTown;
    private String addressStreet;
    private String addressHouseNumber;
    private String[] activities;
    private String name;

    public CompanyBuilder setIco(String ico) {
        this.ico = ico;
        return this;
    }

    public CompanyBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CompanyBuilder setAddressState(String addressState) {
        this.addressState = addressState;
        return this;
    }

    public CompanyBuilder setAddressTown(String addressTown) {
        this.addressTown = addressTown;
        return this;
    }

    public CompanyBuilder setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
        return this;
    }

    public CompanyBuilder setAddressHouseNumber(String addressHouseNumber) {
        this.addressHouseNumber = addressHouseNumber;
        return this;
    }


    public CompanyBuilder setActivities(String[] activities) {
        this.activities = activities;
        return this;
    }

    public Company create() {
        return new Company(ico, name, addressState, addressTown, addressStreet, addressHouseNumber, activities);
    }
}
