package cz.janousek.work.ares.ui.entryform;

import android.app.Activity;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;

import cz.janousek.work.ares.R;
import cz.janousek.work.ares.ui.entryform.listener.SearchButtonListener;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public class Renderer {
    private SearchButtonListener searchButtonListener;
    private ViewHolder views;
    private Activity activity;

    public Renderer(Activity activity) {
        this.activity = activity;
        init();
    }

    public void render() {
        setButtonOnClickListener();
    }

    public void renderError(String errorMessage) {
        views.tilIcoInput.setError(errorMessage);
    }

    private void init() {
        views = new ViewHolder(activity);
    }

    private void setButtonOnClickListener() {
        views.bSearch.setOnClickListener(searchButtonListener);
    }

    public String getIco() {
        return views.edIcoInput.getText().toString();
    }

    public void setSearchButtonListener(SearchButtonListener searchButtonListener) {
        this.searchButtonListener = searchButtonListener;
    }

    private class ViewHolder {

        private Button bSearch;
        private EditText edIcoInput;
        private TextInputLayout tilIcoInput;

        public ViewHolder(Activity activity) {
            init(activity);
        }

        private void init(Activity activity) {
            bSearch = activity.findViewById(R.id.b_search);
            edIcoInput = activity.findViewById(R.id.ed_ico_input);
            tilIcoInput = activity.findViewById(R.id.til_input_ico);
        }
    }
}
