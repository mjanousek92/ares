package cz.janousek.work.ares.ui.companydetail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.sql.SQLException;

import cz.janousek.work.ares.AresApplication;
import cz.janousek.work.ares.R;
import cz.janousek.work.ares.model.database.Company;

public class CompanyDetailActivity extends AppCompatActivity {

    public static final String INTENT_ICO = "ICO";

    private AresApplication ares;
    private Factories factories;
    private Renderer renderer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ares = (AresApplication) getApplication();
        factories = new Factories(this, ares);
        renderer = factories.getRenderer();

        renderContent();
    }

    private void renderContent() {
        try {
            renderCompany();
        } catch (SQLException e) {
            renderEmptyContent();
        }
    }

    private void renderEmptyContent() {
        renderer.renderEmpty();
    }

    private void renderCompany() throws SQLException {
        String ico = getIcoFromIntent();
        Company company = getLoadCompanyFromDatabase(ico);
        render(company);
    }

    private String getIcoFromIntent() {
        return getIntent().getStringExtra(INTENT_ICO);
    }

    private Company getLoadCompanyFromDatabase(String ico) throws SQLException {
        return ares.getDatabaseHelper().getCompanyDao().get(ico);
    }

    private void render(Company company) {
        renderer.render(company);
    }
}
