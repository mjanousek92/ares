package cz.janousek.work.ares.ui.entryform;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import cz.janousek.work.ares.AresApplication;
import cz.janousek.work.ares.R;

public class EntryFormActivity extends AppCompatActivity {

    private AresApplication ares;
    private Factories factories;
    private Renderer renderer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_form);

        ares = (AresApplication) getApplication();
        factories = new Factories(this, ares);
        render();
    }

    private void render() {
        renderer = factories.getRenderer();
        renderer.setSearchButtonListener(factories.getSearchButtonListener());
        renderer.render();
    }
}
