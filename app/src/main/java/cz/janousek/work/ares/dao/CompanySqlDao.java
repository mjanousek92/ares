package cz.janousek.work.ares.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import cz.janousek.work.ares.model.database.Company;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public class CompanySqlDao extends BaseDaoImpl<Company, String> implements CompanyDao {

    public CompanySqlDao(
        ConnectionSource connectionSource
    ) throws SQLException {
        super(connectionSource, Company.class);
    }


    @Override
    public Company get(String id) throws SQLException {
        Company company = queryForId(id);
        if (company != null) {
            return company;
        } else {
            throw new NotFoundException();
        }
    }

    @Override
    public void save(Company company) {
        try {
            create(company);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public class NotFoundException extends RuntimeException {

    }
}
