package cz.janousek.work.ares.ui.entryform;

import android.app.Activity;

import cz.janousek.work.ares.AresApplication;
import cz.janousek.work.ares.service.CompanyCallback;
import cz.janousek.work.ares.service.CompanyService;
import cz.janousek.work.ares.ui.entryform.listener.SearchButtonListener;

/**
 * Created by Martin Janousek on 10.2.18.
 * - email: mjanousek92@gmail.com
 */

public class Factories {

    private final Activity activity;
    private final AresApplication ares;

    private Renderer renderer;
    private SearchButtonListener searchButtonListener;
    private CompanyService companyService;
    private CompanyCallback companyCallback;

    public Factories(Activity activity, AresApplication ares) {
        this.activity = activity;
        this.ares = ares;
    }

    public Renderer getRenderer() {
        if (renderer == null) {
            renderer = new Renderer(activity);
        }
        return renderer;
    }

    public SearchButtonListener getSearchButtonListener() {
        if(searchButtonListener == null) {
            searchButtonListener = new SearchButtonListener(getCompanyService(), getRenderer());
        }
        return searchButtonListener;
    }

    private CompanyService getCompanyService() {
        if (companyService == null) {
            companyService = new CompanyService(ares, getCompanyCallback());
        }
        return companyService;
    }

    public CompanyCallback getCompanyCallback() {
        if (companyCallback == null) {
            companyCallback = new CompanyCallback(activity, getRenderer());
        }
        return companyCallback;
    }
}
