package cz.janousek.work.ares;

import android.app.Application;
import android.support.annotation.NonNull;

import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;

import cz.janousek.work.ares.api.Api;
import cz.janousek.work.ares.api.ApiConstants;
import cz.janousek.work.ares.database.DatabaseHelper;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by Martin Janousek on 9.2.18.
 * - email: mjanousek92@gmail.com
 */

public class AresApplication extends Application{

    private Retrofit retrofit;
    private Api api;
    private DatabaseHelper databaseHelper;

    public Retrofit getRetrofitBuilder(){
        if(retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(ApiConstants.SERVER_URL)
                    .addConverterFactory(getConverterFactory())
                    .build();
        }
        return retrofit;
    }

    @NonNull
    private SimpleXmlConverterFactory getConverterFactory() {
        return SimpleXmlConverterFactory.createNonStrict(new Persister(new AnnotationStrategy()));
    }

    public Api getApi(){
        if(api == null){
            api = getRetrofitBuilder().create(Api.class);
        }
        return api;
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(getApplicationContext());
        }
        return databaseHelper;
    }

}
